import data from '../data/data';

export function getData (data) {

    return {
        type: 'DATA_LOADED',
        payload: data
    }
}

export function loadData() {
    return function (dispatch, getState) {
        return data.getData()
            .then((response) => response.json())
            .then((data) => dispatch(getData(data)))
            .catch((err) => console.log(err));
    };
}