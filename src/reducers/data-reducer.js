export default function (state = {}, action) {
    let data = action.payload;

    console.log("______", state, data);

    switch (action.type) {
        case 'DATA_LOADED':
            return {
                ...state,
                data
            };

        default:
        return state
    }

}