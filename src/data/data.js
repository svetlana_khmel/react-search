const dataUrl = '/data.json';

const data = {
    getData() {
        return fetch(dataUrl);
    }
};

export default data;
