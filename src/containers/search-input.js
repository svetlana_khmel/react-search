import React, { Component } from 'react';

export default class SearchInput extends Component {
    constructor (props) {
        super(props);

        this.doSearch = this.doSearch.bind(this);
    }

    doSearch (e) {
        this.props.doSearch(e.target.value);
    }

    render () {
        return (
            <input type="text" placeholder="Search" onChange={this.doSearch} />
        )
    }
}