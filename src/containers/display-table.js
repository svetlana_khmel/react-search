import React, { Component } from 'react';

export default class DisplayTable extends Component {
    constructor (props) {
        super(props);

        this.props = props;
    }

    render () {
        let rows = [];

        if (this.props.data) {
            this.props.data.forEach(function (data, index) {
                rows.push(<div key={index}>{data.name}</div>);
            });
        }

        return (
            <div className="data-table">Some Data  {rows} </div>
        )
    }
}