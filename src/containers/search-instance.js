import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/get-data';
import { bindActionCreators } from 'redux';

import SearchInput from './search-input';
import DisplayTable from './display-table';

class SearchInstance extends Component {
    constructor (props) {
        super(props);

        this.state = {
            query: '',
            data: null,
            dataJSON: ''
        };

        this.doSearch = this.doSearch.bind(this);
    }

    componentWillMount() {
        this.props.actions.loadData().then((res)=>{
            console.log("PROPS ", this.props);

            this.setState({
                   data: true,
                   dataJSON: this.props.data.data
                });
        });

    }

    componentWillReceiveProps(next) {
        console.log("++++ ", next);
    }
    //
    // loadData () {
    //     var url = '../data/data.json';
    //
    //     var xhr = new XMLHttpRequest();
    //     xhr.open("GET", url, true);
    //
    //     //Send the proper header information along with the request
    //     xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //
    //     xhr.onreadystatechange = function () {
    //         //Call a function when the state changes.
    //         if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
    //             this.setState({
    //                data: true,
    //                dataJSON: JSON.parse(xhr.responseText)
    //             });
    //             return;
    //         }
    //
    //         if (xhr.status != 200) {
    //             this.setState({
    //                 loginStatus: 'failed',
    //                 loginNegativeMessage: 'Login failed. Check your credentials or connection.'
    //             });
    //             console.log('XMLhttpRequest error', xhr.statusText, 'XMLhttpRequest ... ', xhr);
    //         }
    //     }.bind(this);
    //
    //     xhr.send();
    // }

    doSearch (queryText) {
        var queryResult = [];

        this.state.dataJSON.forEach(function (person) {
            if(person.name.toLowerCase().indexOf(queryText.toLowerCase())!=-1)
            queryResult.push(person);
        });

        this.setState({
            query: queryText,
            filteredData: queryResult
        });
    }

    render () {

        console.log('render...', this.props);

        if (this.state.data) {
            return (
                <div className="instant-box">
                <h2>Instant search</h2>
                <SearchInput query={this.state.query} doSearch={this.doSearch} />
                <DisplayTable data={this.state.filteredData} />
            </div>
            );
        }

        return <div>Loading...</div>;
    }
}

function mapStateToProps (state) {
    console.log('Search mapsStateToProps', state);

    return {
        data: state.data
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchInstance);