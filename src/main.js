import React from 'react';
import ReactDOM from 'react-dom';
import SearchInstance from './containers/search-instance';

import { Provider } from 'react-redux';
import configureStore from './configureStore';

const store = configureStore();

document.addEventListener('DOMContentLoaded', function() {
    ReactDOM.render(
       <Provider store={store}><SearchInstance /></ Provider>,
        document.getElementById('mount')
    )
});